import {Module} from '@nestjs/common';
import {CommentService} from './comment.service';
import {MongooseModule} from '@nestjs/mongoose';
import {CommentModel} from './comment.model';
import {CommentResolver} from './comment.resolver';
import {CardModule} from '../Card/card.module';
import {ActivityModule} from '../Activity/activity.module';

@Module({
    providers: [CommentService, CommentResolver],
    exports: [CommentService],
    imports: [
        CardModule,
        ActivityModule,
        MongooseModule.forFeature([{name: 'Comment', schema: CommentModel}])],
})
export class CommentModule {
}
