import {Inject, Injectable} from '@nestjs/common';
import {isNull} from 'lodash/fp';
import {Document, Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {CardComment, CreateCardInput, GetOneCardInput, GetOneCommentInput, User} from '../interfaces';
import {CardService} from '../Card/card.service';
import {CardCommentInstance} from './comment.class';

@Injectable()
export class CommentService {
    constructor(@InjectModel('Comment') public readonly model: Model<CardComment & Document>,
                @Inject('CardService') private readonly cardService: CardService,
    ) {
    }

    public async create(user: User & Document, determineCard: GetOneCardInput, mutation: CreateCardInput): Promise<CardCommentInstance> {
        const comment = new this.model({user, ...mutation});
        const cardInstance = await (await this.cardService.one(determineCard)).mutation.pushOne.comment(comment);
        comment.card = cardInstance.instance;
        return new CardCommentInstance(await comment.save());
    }

    public async one(determineComment: GetOneCommentInput): Promise<CardCommentInstance> {
        return new CardCommentInstance(await this.model.findOne(determineComment));
    }
}