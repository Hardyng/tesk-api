import {Args, Mutation, Parent, ResolveProperty, Resolver} from '@nestjs/graphql';
import {CommentService} from './comment.service';
import {ActionType, ActivityType, CardComment, CreateCardInput, GetOneCardInput, GetOneCommentInput, User,} from '../interfaces';
import {Inject, UseGuards} from '@nestjs/common';
import {GqlAuthGuard} from '../auth/gqlAuth.guard';
import {UserDecorator} from '../shared/user.decorator';
import {Document} from 'mongoose';
import {ForbiddenError} from 'apollo-server-errors';
import {ActivityService} from '../Activity/activity.service';

@Resolver('CardComment')
export class CommentResolver {
    constructor(public readonly service: CommentService,
                @Inject('ActivityService') private readonly activityService: ActivityService,
    ) {
    }

    @Mutation()
    @UseGuards(GqlAuthGuard)
    async createComment(@UserDecorator() user: User & Document,
                        @Args('determineCard') determineCard: GetOneCardInput,
                        @Args('mutation') mutation: CreateCardInput,
    ): Promise<CardComment> {
        const commentInstance = await this.service.create(user, determineCard, mutation);
        await this.activityService.createActivity(user, ActivityType.COMMENT, ActionType.CREATE, commentInstance.instance._id);
        return commentInstance.instance;
    }

    @Mutation()
    @UseGuards(GqlAuthGuard)
    async removeComment(@UserDecorator() user: User & Document,
                        @Args('determineComment') determineComment: GetOneCommentInput,
    ): Promise<CardComment> {
        const commentInstance = await this.service.one(determineComment);
        if (commentInstance.instance.user.toString() === user._id.toString()) {
            await commentInstance.mutation.delete();
        } else {
            throw new ForbiddenError('This is not comment of logged in user.');
        }
        await this.activityService.createActivity(user, ActivityType.COMMENT, ActionType.DELETE, String(commentInstance.instance.card));
        return commentInstance.instance;
    }

    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async user(@Parent() parent: CardComment & Document): Promise<User> {
        const populated = await parent.populate('user').execPopulate();
        return populated.user;
    }
}
