import {Schema} from 'mongoose';

export const CommentModel = new Schema({
    card: {
        type: Schema.Types.ObjectId,
        ref: 'Card',
        required: true,
    },
    text: {
        type: String,
        required: true,
        default: 'Default comment',
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    created: {
        type: Date,
        default: Date.now(),
    },
});
