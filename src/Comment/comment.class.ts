import {CardComment} from '../interfaces';
import {Document} from 'mongoose';
import {MongoDocumentInstance} from '../shared/classes/MongoDocumentInstance';

export class CardCommentInstance extends MongoDocumentInstance<CardComment> {
    constructor(protected mongoModel: CardComment & Document) {
        super(mongoModel);
    }
}
