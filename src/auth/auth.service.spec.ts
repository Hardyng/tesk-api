import {Test} from '@nestjs/testing';
import {AuthService} from './auth.service';
import {MockTestingService} from '../mock-testing/mock-testing.service';
import {UserService} from '../User/user.service';
import {UserModel} from '../User/user.model';
import {MongooseModule} from '@nestjs/mongoose';
import {JwtService} from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
jest.setTimeout(20000);
const UserMockService = {
    one: jest.fn(),
    create: jest.fn(),
    pushChild: jest.fn(),
};
const JwtMockService = {
    sign: jest.fn(),
};
describe('AuthService', () => {
    let service: AuthService;

    beforeAll(async () => {
        await MockTestingService.getConfig();
        const module = await Test.createTestingModule({
            providers: [
                AuthService,
                {
                    provide: UserService,
                    useValue: UserMockService,
                },
                {
                    provide: JwtService,
                    useValue: JwtMockService,
                },
            ],
            imports: [
                MongooseModule.forRoot(
                    MockTestingService.config.mongoUri,
                    {
                        useNewUrlParser: true,
                    }),
                MongooseModule.forFeature([
                    {
                        name: 'User',
                        schema: UserModel,
                    },
                ]),
            ],
        }).compile();

        service = module.get<AuthService>(AuthService);
    });
    afterAll(async () => {
        await MockTestingService.endTesting();
    });
    it('should exist', async () => {
        expect(service).toBeDefined();
    });
    it('should validate user', async () => {
        const payload = {
            email: 'abcdef@gmail.com',
        };
        const user = {
            email: 'abcdef@gmail.com',
            password: '1fgeshrthhh3trdhtrh',
        };
        UserMockService.one = jest.fn().mockImplementation(data => {
            if (data.email === user.email) {
                return user;
            }
            return null;
        });
        expect(await service.validateUser(payload)).toEqual(user);
    });
    it('should not validate user', async () => {
        const payload = {
            email: 'qqqqqq@gmail.com',
        };
        const user = {
            email: 'abcdef@gmail.com',
            password: '1fgeshrthhh3trdhtrh',
        };
        UserMockService.one = jest.fn().mockImplementation(data => {
            if (data.email === user.email) {
                return user;
            }
            return null;
        });
        expect(await service.validateUser(payload)).toEqual(null);
    });
    it('should register', async () => {
        const credentials = {
            username: '123',
            email: 'testowy@gmail.com',
            password: '123456',
        };
        UserMockService.create = jest.fn().mockImplementation(payload => {
            return {};
        });
        JwtMockService.sign = jest.fn().mockImplementation(payload => {
            return 'token 123';
        });
        expect(await service.signup(credentials)).toEqual('token 123');
    });
    it('should not register', async () => {
        const credentials = {
            username: '123',
            email: 'testowy@gmail.com',
            password: '123456',
        };
        UserMockService.create = jest.fn().mockImplementation(payload => {
            throw new Error('abc');
        });
        JwtMockService.sign = jest.fn().mockImplementation(payload => {
            return 'token 123';
        });
        try {
            await service.signup(credentials);
        } catch (e) {
            expect(true).toBeTruthy();
        }
    });
    it('should login', async () => {
        const credentials = {
            email: 'test',
            password: '123',
        };

        const hashedPassword = await bcrypt.hash('123', 1);
        UserMockService.one = jest.fn().mockImplementation(payload => {
            return {
                email: 'test',
                password: hashedPassword,
            };
        });
        JwtMockService.sign = jest.fn().mockImplementation(payload => {
            return 'token 123';
        });
        expect(await service.signIn(credentials)).toEqual('token 123');
    });
    it('should not login', async () => {
        const credentials = {
            email: 'test',
            password: '123',
        };

        const hashedPassword = await bcrypt.hash('123', 1);
        UserMockService.one = jest.fn().mockImplementation(payload => {
            return null;
        });
        JwtMockService.sign = jest.fn().mockImplementation(payload => {
            return 'token 123';
        });
        try {
            await service.signIn(credentials);
        } catch (e) {
            expect(true).toBeTruthy();
        }
    });
});
