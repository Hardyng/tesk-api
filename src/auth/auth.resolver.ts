import {Args, Context, Mutation, Query, Resolver} from '@nestjs/graphql';
import {UserService} from '../User/user.service';
import {LoginInput, SignupInput, User} from '../interfaces';
import {UseGuards} from '@nestjs/common';
import {GqlAuthGuard} from './gqlAuth.guard';
import {AuthService} from './auth.service';
import {omitPassword} from '../User/user.resolver';
import {UserDecorator} from '../shared/user.decorator';
import {Document} from "mongoose";

@Resolver('Auth')
export class AuthResolver {
    constructor(private usersService: UserService,
                private authService: AuthService,
    ) {
    }
    @Query()
    @UseGuards(GqlAuthGuard)
    public token(@UserDecorator() user: User & Document): string {
        return this.authService.createToken({email: user.email});
    }
    @Query()
    @UseGuards(GqlAuthGuard)
    public me(@UserDecorator() user: User & Document): User {
        return omitPassword(user);
    }

    @Mutation()
    public async signup(@Args('credentials') credentials: SignupInput): Promise<string> {
        return await this.authService.signup(credentials);
    }

    @Mutation('login')
    public async login(@Args('credentials') credentials: LoginInput): Promise<string> {
        return this.authService.signIn(credentials);
    }
}
