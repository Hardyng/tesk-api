import {forwardRef, Module} from '@nestjs/common';
import {AuthService} from './auth.service';
import {UserModule} from '../User/user.module';
import {PassportModule} from '@nestjs/passport';
import {JwtModule} from '@nestjs/jwt';
import {JwtStrategy} from './jwt.strategy';
import {AuthResolver} from './auth.resolver';

if (!process.env['SECRET']) {
    require('dotenv').config();
}

@Module({
    imports: [
        PassportModule.register({session: true, defaultStrategy: 'jwt'}),
        JwtModule.register({
            secretOrPrivateKey: process.env['SECRET'],
            signOptions: {
                expiresIn: 3600,
            },
        }),
        forwardRef(() => UserModule),
    ],
    providers: [
        AuthService,
        JwtStrategy,
        AuthResolver,
    ],
})
export class AuthModule {
}