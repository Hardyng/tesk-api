import {Injectable} from '@nestjs/common';
import {UserService} from '../User/user.service';
import {JwtService} from '@nestjs/jwt';
import {JwtTokenPayload} from './JWTTokenPayload.interface';
import {LoginInput, SignupInput, User} from '../interfaces';
import * as bcrypt from 'bcrypt';
import {UserInputError} from 'apollo-server-errors';

@Injectable()
export class AuthService {

    constructor(private readonly usersService: UserService,
                private readonly jwtService: JwtService,
    ) {
    }

    async signup(payload: SignupInput): Promise<string> {
        const hashedPassword = await bcrypt.hash(payload.password, 10);
        const createdUser = await this.usersService.create({
            firstName: payload.firstName,
            lastName: payload.lastName,
            username: payload.username,
            email: payload.email,
            password: hashedPassword,
        });
        return this.createToken({email: createdUser.instance.email});
    }

    async signIn(credentials: LoginInput): Promise<string> {
        const user = await this.usersService.one({email: credentials.email});
        if (!user) {
            throw new UserInputError('No user with that email', {
                invalidArgs: ['email'],
            });
        }
        const valid = await bcrypt.compare(credentials.password, user.instance.password);
        if (!valid) {
            throw new UserInputError('Invalid password', {
                invalidArgs: ['password'],
            });
        }
        return this.createToken({email: user.instance.email});
    }

    async validateUser(payload: JwtTokenPayload): Promise<User> {
        const user = await this.usersService.one({email: payload.email});
        return await user.instance;
    }
    public createToken(payload: JwtTokenPayload): string {
        return this.jwtService.sign(payload);
    }
}