import {Args, Mutation, Parent, ResolveProperty, Resolver} from '@nestjs/graphql';
import {CardService} from './card.service';
import {Activity, Attachment, Card, CardComment, CreateCardInput, GetOneBoardInput, GetOneCardInput, Label, UpdateCardInput} from '../interfaces';
import {UseGuards} from '@nestjs/common';
import {GqlAuthGuard} from '../auth/gqlAuth.guard';
import {Document} from 'mongoose';

@Resolver('Card')
export class CardResolver {
    constructor(public readonly service: CardService) {
    }

    @Mutation()
    @UseGuards(GqlAuthGuard)
    async createCard(@Args('determineCardList') determineCardList: GetOneBoardInput, @Args('mutation') mutation: CreateCardInput): Promise<Card> {
        return (await this.service.create(determineCardList, mutation)).instance;
    }
    // UpdateCardInput
    @Mutation()
    @UseGuards(GqlAuthGuard)
    async updateCard(@Args('mutation') mutation: UpdateCardInput): Promise<Card> {
        return (await this.service.one({_id: mutation._id})).instance;
    }
    @Mutation()
    @UseGuards(GqlAuthGuard)
    async removeCard(@Args('determineCard') determineCard: GetOneCardInput): Promise<Card> {
        return (await (await this.service.one(determineCard)).mutation.delete()).instance;
    }
    @Mutation()
    @UseGuards(GqlAuthGuard)
    async toggleLabel(@Args('determine') determineCard: GetOneCardInput, @Args('toggleLabelId') toggleLabelId: string): Promise<Card> {
        const cardInstance = await this.service.one(determineCard);
        if (cardInstance.instance.labels.find(_label => String(_label) === toggleLabelId)) {
            return (await cardInstance.mutation.pullOne.label({_id: toggleLabelId})).instance;
        } else {
            return (await cardInstance.mutation.pushOne.label({_id: toggleLabelId})).instance;
        }
    }
    @Mutation()
    @UseGuards(GqlAuthGuard)
    async setCoverAttachment(@Args('cardId') cardId: string, @Args('attachmentId') attachmentId: string): Promise<Card> {
        const cardInstance = await this.service.one({_id: cardId});
        return (await cardInstance.mutation.update.coverAttachment({_id: attachmentId})).instance;
    }

    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async comments(@Parent() parent: Card & Document): Promise<CardComment[]> {
        const populated = await parent.populate('comments').execPopulate();
        return populated.comments;
    }

    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async labels(@Parent() parent: Card & Document): Promise<Label[]> {
        const populated = await parent.populate('labels').execPopulate();
        return populated.labels;
    }

    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async checklists(@Parent() parent: Card & Document): Promise<Label[]> {
        const populated = await parent.populate('checklists').execPopulate();
        return populated.checklists;
    }
    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async attachments(@Parent() parent: Card & Document): Promise<Attachment[]> {
        const populated = await parent.populate('attachments').execPopulate();
        return populated.attachments;
    }
    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async activities(@Parent() parent: Card & Document): Promise<Activity[]> {
        const populated = await parent.populate('activities').execPopulate();
        return populated.activities;
    }
}
