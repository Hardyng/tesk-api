import {Inject, Injectable} from '@nestjs/common';
import {isNull} from 'lodash/fp';
import {Activity, Attachment, Card, CardComment, CheckList, CreateCardInput, GetOneCardInput, GetOneCardListInput,} from '../interfaces';
import {Document, Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {CardListService} from '../CardList/cardList.service';
import {CardInstance} from './card.class';

@Injectable()
export class CardService {
    constructor(@InjectModel('Card') public readonly model: Model<Card & Document>,
                @Inject('CardListService') private readonly cardListService: CardListService,
    ) {
    }

    public async create(determineCardList: GetOneCardListInput, cardInput: CreateCardInput): Promise<CardInstance> {
        const card = new this.model(cardInput);
        const cardListInstance = await this.cardListService.one(determineCardList);
        await cardListInstance.mutation.pushOne.card(card);
        return new CardInstance(await card.save());
    }

    public async one(determineCard: GetOneCardInput): Promise<CardInstance> {
        console.log(determineCard);
        return new CardInstance(await this.model.findOne(determineCard));
    }

    // public async update(mutation: UpdateCardInput): Promise<Card> {
    //     return await this.model.findOneAndUpdate({_id: mutation._id}, mutation, {new: true});
    // }

    // public async delete(determineCard: GetOneCardInput): Promise<Card> {
    //     return await this.model.findOneAndDelete(determineCard).exec();
    // }

    public async removeMany(cards: Card[]): Promise<Card> {
        return await this.model.deleteMany(
            {
                _id: {$in: cards},
            },
        );
    }

    public async toggleLabel(determineCard: GetOneCardInput, toggleLabelId: string): Promise<Card> {
        const card = await this.model.findById(determineCard._id);
        const labelExists = !!card.labels.find(_label => String(_label) === toggleLabelId);
        if (labelExists) {
            return await this.model.findByIdAndUpdate(determineCard._id,
                {
                    $pull: {
                        labels: toggleLabelId,
                    },
                },
                {
                    new: true,
                });
        } else {
            return await this.model.findByIdAndUpdate(determineCard._id,
                {
                    $push: {
                        labels: toggleLabelId,
                    },
                },
                {
                    new: true,
                });
        }
    }

    public async addComment(determine: GetOneCardInput, comment: CardComment): Promise<Card> {
        return await this.model.findOneAndUpdate(determine, {
            $push: {
                comments: comment,
            },
        }).exec();
    }

    public async addCheckList(determine: GetOneCardInput, checkList: CheckList): Promise<Card> {
        return await this.model.findOneAndUpdate(determine, {
            $push: {
                checklists: checkList,
            },
        }).exec();
    }

    public async addAttachment(cardId: string, attachment: Attachment): Promise<Card> {
        return await this.model.findByIdAndUpdate(cardId, {
            $push: {
                attachments: attachment,
            },
        }, {
            new: true,
        }).exec();
    }
    public async setCoverAttachment(cardId: string, attachmentId: string): Promise<Card> {
        return await this.model.findByIdAndUpdate(cardId, {
            $set: {
                coverAttachment: attachmentId,
            },
        }, {
            new: true,
        }).populate('coverAttachment').exec();
    }
    public async addActivity(cardId: string, activity: Activity): Promise<Card> {
        return await this.model.findByIdAndUpdate(cardId, {
            $push: {
                activities: activity,
            },
        }, {
            new: true,
        }).exec();
    }
}