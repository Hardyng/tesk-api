import {Schema} from 'mongoose';

export const CardModel = new Schema({
    name: {
        type: String,
        default: 'Untitled Card List',
    },
    description: {
        type: String,
        default: 'Default card description',
    },
    coverAttachment: {
        type: Schema.Types.ObjectId,
        ref: 'Attachment',
    },
    attachments: [{
        type: Schema.Types.ObjectId,
        ref: 'Attachment',
    }],
    members: [{
        type: Schema.Types.ObjectId,
        ref: 'User',
    }],
    labels: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Label',
        },
    ],
    checklists: [
        {
            type: Schema.Types.ObjectId,
            ref: 'CheckList',
            default: [],
        },
    ],
    checkItemsNumber: {
        type: Number,
        default: 0,
    },
    checkItemsCheckedNumber: {
        type: Number,
        default: 0,
    },
    comments: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Comment',
            default: [],
        },
    ],
    due: [
        {
            type: Date,
            default: Date.now,
        },
    ],
    activities: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Activity',
            default: [],
        },
    ],
});
