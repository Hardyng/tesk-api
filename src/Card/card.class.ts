import {Attachment, Card, Label} from '../interfaces';
import {Document} from 'mongoose';
import {MongoDocumentInstance} from '../shared/classes/MongoDocumentInstance';

export class CardInstance extends MongoDocumentInstance<Card> {
    readonly mutation = {
        delete: async (): Promise<CardInstance> => {
            await this.mongoModel.remove();
            return this;
        },
        update: {
            instance: async (obj: Card): Promise<CardInstance> => {
                await this.mongoModel.update(obj);
                return this;
            },
            coverAttachment: async (cover: Attachment): Promise<CardInstance> => {
                await this.mongoModel.update({$set: {coverAttachment: cover._id}});
                return this;
            },
        },
        pushOne: {
            label: async (obj: Label): Promise<CardInstance> => {
                await this.mongoModel.update({$push: {labels: obj}});
                return this;
            },
            comment: async (obj: Comment | Document): Promise<CardInstance> => {
                await this.mongoModel.update({$push: {comments: obj}});
                return this;
            },
            attachment: async (obj: Attachment): Promise<CardInstance> => {
                await this.mongoModel.update({$push: {attachments: obj}});
                return this;
            },
        },
        pullOne: {
            label: async (obj: Label): Promise<CardInstance> => {
                await this.mongoModel.update({$pull: {labels: obj}});
                return this;
            },
        },
    };

    constructor(protected mongoModel: Card & Document) {
        super(mongoModel);
    }
}
