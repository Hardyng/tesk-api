import {forwardRef, Module} from '@nestjs/common';
import {CardService} from './card.service';
import {MongooseModule} from '@nestjs/mongoose';
import {CardModel} from './card.model';
import {CardResolver} from './card.resolver';
import {CardListModule} from '../CardList/cardList.module';
import {UserModule} from '../User/user.module';

@Module({
    providers: [CardService, CardResolver],
    exports: [CardService],
    imports: [
        // UserModule,
        forwardRef(() => CardListModule),
        MongooseModule.forFeature([{name: 'Card', schema: CardModel}])],
})
export class CardModule {
}
