import {Inject, Injectable} from '@nestjs/common';
import * as mongoose from 'mongoose';
import {Document, Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {UserService} from '../User/user.service';
import {isNull} from 'lodash/fp';
import {Board, CreateBoardInput, GetOneBoardInput, User} from '../interfaces';
import {ServiceMethods} from '../shared/interfaces/serviceMethods';
import {BoardInstance} from './board.class';

@Injectable()
export class BoardService implements ServiceMethods<BoardInstance> {
    constructor(@InjectModel('Board') public readonly model: Model<Board & Document>,
                @Inject('UserService') private readonly userService: UserService,
    ) {
    }

    public async one(determine: GetOneBoardInput): Promise<BoardInstance> {
        return new BoardInstance(await this.model.findOne(determine));
    }

    public async all(user: User & Document): Promise<BoardInstance[]> {
        const userPopulated = await user.populate('boards').execPopulate();
        return userPopulated.boards.map((b: Board & Document) => new BoardInstance(b));
    }

    public async create(user: User & Document, objectToCreate: CreateBoardInput): Promise<BoardInstance> {
        const uniqUri = new mongoose.mongo.ObjectId();
        const board = new this.model({...objectToCreate, uri: `${objectToCreate.name.toLowerCase().split(' ').join('-')}-${uniqUri}`, users: [user]});
        const _user = await this.userService.one(user);
        await _user.mutation.pushOne.board(board);
        return new BoardInstance(await board.save());
    }
}