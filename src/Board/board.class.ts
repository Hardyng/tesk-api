import {Board, CardList, Label} from '../interfaces';
import {Document} from 'mongoose';
import {MongoDocumentInstance} from '../shared/classes/MongoDocumentInstance';

export class BoardInstance extends MongoDocumentInstance<Board> {
    readonly mutation = {
        delete: async (): Promise<BoardInstance> => {
            await this.mongoModel.remove();
            return this;
        },
        update: {
            instance: async (obj: Board): Promise<BoardInstance> => {
                await this.mongoModel.update(obj);
                return this;
            },
        },
        pushOne: {
            list: async (obj: CardList): Promise<BoardInstance> => {
                await this.mongoModel.update({$push: {lists: obj}});
                return this;
            },
            label: async (obj: Label): Promise<BoardInstance> => {
                await this.mongoModel.update({$push: {labels: obj}});
                return this;
            },
        },
    };
    constructor(protected mongoModel: Board & Document) {
        super(mongoModel);
    }
}
