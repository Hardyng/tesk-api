import {Schema} from 'mongoose';

export const BoardModel = new Schema({
    name: {
        type: String,
        required: true,
        default: 'Untitled Board',
    },
    uri: {
        type: String,
        required: true,
        unique: true,
    },
    users: [{
        type: Schema.Types.ObjectId,
        ref: 'User',
        default: [],
    }],
    lists: [{
        type: Schema.Types.ObjectId,
        ref: 'CardList',
        default: [],
    }],
    labels: [{
        type: Schema.Types.ObjectId,
        ref: 'Label',
        default: [],
    }],
});