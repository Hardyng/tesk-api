import {Args, Mutation, Parent, Query, ResolveProperty, Resolver} from '@nestjs/graphql';
import {BoardService} from './board.service';
import {Board, CardList, CreateBoardInput, GetOneBoardInput, Label, UpdateBoardInput, User} from '../interfaces';
import {Document} from 'mongoose';
import {UseGuards} from '@nestjs/common';
import {GqlAuthGuard} from '../auth/gqlAuth.guard';
import {UserDecorator} from '../shared/user.decorator';
import {BoardInstance} from './board.class';

@Resolver('Board')
export class BoardResolver {
    constructor(public readonly service: BoardService) {
    }

    @Query()
    @UseGuards(GqlAuthGuard)
    public async allBoards(@UserDecorator() user: User & Document): Promise<Board[]> {
        return (await this.service.all(user)).map((_bi: BoardInstance) => _bi.instance);
    }

    // TODO: Add guard to allow only getting board user has access to
    @Query()
    @UseGuards(GqlAuthGuard)
    public async oneBoard(@Args('determine') determine: GetOneBoardInput): Promise<Board> {
        return (await this.service.one(determine)).instance;
    }
    @Mutation()
    @UseGuards(GqlAuthGuard)
    public async createBoard(@UserDecorator() user: User & Document, @Args('mutation') mutation: CreateBoardInput): Promise<Board> {
        return (await this.service.create(user, mutation)).instance;
    }
    @Mutation()
    @UseGuards(GqlAuthGuard)
    public async updateBoard(@Args('determine') determine: GetOneBoardInput, @Args('mutation') mutation: UpdateBoardInput): Promise<Board> {
        return (await (await this.service.one(determine)).mutation.update.instance(mutation)).instance;
    }
    @Mutation()
    @UseGuards(GqlAuthGuard)
    public async removeBoard(@Args('determine') determine: GetOneBoardInput): Promise<Board> {
        const board = await this.service.one(determine);
        console.log(board);
        return (await board.mutation.delete()).instance;
    }
    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async users(@Parent() parent: Board & Document): Promise<User[]> {
        const populated = await parent.populate('users').execPopulate();
        return populated.users;
    }

    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async lists(@Parent() parent: Board & Document): Promise<CardList[]> {
        const populated = await parent.populate('lists').execPopulate();
        return populated.lists;
    }
    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async labels(@Parent() parent: Board & Document): Promise<Label[]> {
        const populated = await parent.populate('labels').execPopulate();
        return populated.labels;
    }
    // TODO: Update board
}
