import {Module} from '@nestjs/common';
import {BoardService} from './board.service';
import {MongooseModule} from '@nestjs/mongoose';
import {BoardModel} from './board.model';
import {BoardResolver} from './board.resolver';
import {UserModule} from '../User/user.module';

@Module({
    providers: [BoardService, BoardResolver],
    exports: [BoardService],
    imports: [
        UserModule,
        MongooseModule.forFeature([{name: 'Board', schema: BoardModel}])],
})
export class BoardModule {
}
