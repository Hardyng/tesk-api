import {Inject, Injectable} from '@nestjs/common';
import {Document, Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {isNull} from 'lodash/fp';
import {CheckList, GetOneCardInput} from '../interfaces';
import {CardService} from '../Card/card.service';
import {CheckListInstance} from './checkList.class';

@Injectable()
export class CheckListService {
    constructor(@InjectModel('CheckList') public readonly model: Model<CheckList & Document>,
                @Inject('CardService') public readonly cardService: CardService,
    ) {
    }

    public async create(card: GetOneCardInput, title: string): Promise<CheckListInstance> {
        const checklist = new this.model({title});
        await this.cardService.addCheckList(card, checklist);
        return new CheckListInstance(await checklist.save());
    }

    public async one(determine: any): Promise<CheckListInstance> {
        return new CheckListInstance(await this.model.findOne(determine));
    }

}