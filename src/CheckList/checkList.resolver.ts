import {Args, Mutation, Parent, ResolveProperty, Resolver} from '@nestjs/graphql';
import {CheckListService} from './checkList.service';
import {UseGuards} from '@nestjs/common';
import {GqlAuthGuard} from '../auth/gqlAuth.guard';
import {CheckList, GetOneCardListInput, Task} from '../interfaces';
import {Document} from 'mongoose';

@Resolver('CheckList')
export class CheckListResolver {
    constructor(public readonly service: CheckListService) {
    }
    @Mutation()
    @UseGuards(GqlAuthGuard)
    async addCheckList(@Args('card') card: GetOneCardListInput, @Args('title') title: string): Promise<CheckList> {
        return (await this.service.create(card, title)).instance;
    }
    @Mutation()
    @UseGuards(GqlAuthGuard)
    async removeCheckList(@Args('id') id: string): Promise<CheckList> {
        return (await (await this.service.one({_id: id})).mutation.delete()).instance;
    }
    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async tasks(@Parent() parent: CheckList & Document): Promise<Task[]> {
        const populatedParent = await parent.populate('tasks').execPopulate();
        return populatedParent.tasks;
    }
}
