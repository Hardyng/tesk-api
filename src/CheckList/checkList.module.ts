import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {CheckListModel} from './checkList.model';
import {CheckListService} from './checkList.service';
import {CheckListResolver} from './checkList.resolver';
import {CardModule} from '../Card/card.module';

@Module({
    providers: [CheckListService, CheckListResolver],
    exports: [CheckListService],
    imports: [
        CardModule,
        MongooseModule.forFeature([{name: 'CheckList', schema: CheckListModel}])],
})
export class CheckListModule {
}
