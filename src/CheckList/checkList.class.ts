import {CheckList, Task} from '../interfaces';
import {Document} from 'mongoose';
import {MongoDocumentInstance} from '../shared/classes/MongoDocumentInstance';

export class CheckListInstance extends MongoDocumentInstance<CheckList> {
    readonly mutation = {
        delete: async (): Promise<CheckListInstance> => {
            await this.mongoModel.remove();
            return this;
        },
        update: {
            instance: async (obj: CheckList): Promise<CheckListInstance> => {
                await this.mongoModel.update(obj);
                return this;
            },
        },
        pushOne: {
            task: async (obj: Task): Promise<CheckListInstance> => {
                await this.mongoModel.update({$push: {tasks: obj}});
                return this;
            },
        },
    };

    constructor(protected mongoModel: CheckList & Document) {
        super(mongoModel);
    }
}
