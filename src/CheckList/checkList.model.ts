import {Schema} from 'mongoose';

export const CheckListModel = new Schema({
    name: {
        type: String,
        default: 'Untitled Checklist',
    },
    tasks: [{
        type: Schema.Types.ObjectId,
        ref: 'Task',
    }],
    tasksFinishedNumber: {
        type: Number,
        default: 0,
    },
});