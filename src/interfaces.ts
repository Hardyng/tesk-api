export enum ActionType {
    CREATE = "CREATE",
    DELETE = "DELETE",
    UPDATE = "UPDATE"
}

export enum ActivityType {
    COMMENT = "COMMENT",
    ATTACHMENT = "ATTACHMENT"
}

export enum AttachmentType {
    IMAGE = "IMAGE",
    LINK = "LINK"
}

export interface CreateBoardInput {
    name: string;
    uri: string;
}

export interface CreateCardInput {
    name?: string;
}

export interface CreateCommentInput {
    _id?: string;
    text?: string;
}

export interface CreateLabelInput {
    name: string;
    color: string;
}

export interface DetermineUserInput {
    username?: string;
    email?: string;
    createdAt?: string;
    firstName?: string;
    lastName?: string;
}

export interface GetOneBoardInput {
    _id?: string;
    uri?: string;
}

export interface GetOneCardInput {
    _id?: string;
}

export interface GetOneCardListInput {
    _id?: string;
}

export interface GetOneCommentInput {
    _id?: string;
}

export interface LoginInput {
    email: string;
    password: string;
}

export interface SignupInput {
    firstName: string;
    lastName: string;
    username: string;
    email: string;
    password: string;
}

export interface UpdateBoardInput {
    name: string;
    uri: string;
}

export interface UpdateCardInput {
    _id?: string;
    name?: string;
    description?: string;
    coverAttachment?: string;
}

export interface UpdateLabelInput {
    _id?: string;
    name?: string;
    color?: string;
}

export interface UpdateTaskInput {
    name?: string;
    done?: boolean;
}

export interface Activity {
    user?: User;
    date?: string;
    type?: ActivityType;
    action?: ActionType;
}

export interface Attachment {
    _id?: string;
    name?: string;
    url?: string;
    created?: string;
    type?: AttachmentType;
    card?: Card;
}

export interface Board {
    _id?: string;
    uri?: string;
    name?: string;
    lists?: CardList[];
    users?: User[];
    labels?: Label[];
}

export interface Card {
    _id?: string;
    name?: string;
    description?: string;
    coverAttachment?: Attachment;
    attachments?: Attachment[];
    activities?: Activity[];
    members?: User[];
    labels?: Label[];
    checklists?: CheckList[];
    checkItemsNumber?: number;
    checkItemsCheckedNumber?: number;
    comments?: CardComment[];
    due?: string;
}

export interface CardComment {
    _id?: string;
    card?: Card;
    text?: string;
    user?: User;
    date?: string;
}

export interface CardList {
    _id?: string;
    name?: string;
    cards?: Card[];
}

export interface CheckList {
    _id: string;
    name: string;
    tasks?: Task[];
    tasksFinishedNumber?: number;
}

export interface Label {
    _id?: string;
    name?: string;
    color?: string;
}

export interface IMutation {
    createAttachment(cardId: string, name: string, url: string): Attachment | Promise<Attachment>;
    deleteAttachment(attachmentId: string): Attachment | Promise<Attachment>;
    signup(credentials: SignupInput): string | Promise<string>;
    login(credentials: LoginInput): string | Promise<string>;
    createBoard(mutation: CreateBoardInput): Board | Promise<Board>;
    updateBoard(determine: GetOneBoardInput, mutation: UpdateBoardInput): Board | Promise<Board>;
    removeBoard(determine: GetOneBoardInput): Board | Promise<Board>;
    createCard(determineCardList: GetOneCardListInput, mutation: CreateCardInput): Card | Promise<Card>;
    updateCard(mutation: UpdateCardInput): Card | Promise<Card>;
    removeCard(determineCard: GetOneCardInput): Card | Promise<Card>;
    toggleLabel(determine: GetOneCardInput, toggleLabelId?: string): Card | Promise<Card>;
    setCoverAttachment(cardId: string, attachmentId: string): Card | Promise<Card>;
    addCardList(determineBoard: GetOneBoardInput, name: string): CardList | Promise<CardList>;
    removeCardList(determineCardList: GetOneCardListInput): CardList | Promise<CardList>;
    addCheckList(card: GetOneCardInput, title: string): CheckList | Promise<CheckList>;
    removeCheckList(id: string): CheckList | Promise<CheckList>;
    createComment(determineCard: GetOneCardInput, mutation: CreateCommentInput): CardComment | Promise<CardComment>;
    removeComment(determineComment: GetOneCommentInput): CardComment | Promise<CardComment>;
    createLabel(boardId: string, mutation: CreateLabelInput): Label | Promise<Label>;
    updateLabel(mutation: UpdateLabelInput): Label | Promise<Label>;
    deleteLabel(_id: string): Label | Promise<Label>;
    addTask(checkListId: string, name: string): Task | Promise<Task>;
    updateTask(id: string, mutation: UpdateTaskInput): Task | Promise<Task>;
    removeTask(id: string): Task | Promise<Task>;
}

export interface IQuery {
    me(): User | Promise<User>;
    token(): string | Promise<string>;
    allBoards(): Board[] | Promise<Board[]>;
    oneBoard(determine: GetOneBoardInput): Board | Promise<Board>;
    allUsers(): User[] | Promise<User[]>;
    oneUser(determine: DetermineUserInput): User | Promise<User>;
    temp__(): boolean | Promise<boolean>;
}

export interface Task {
    _id?: string;
    name?: string;
    done?: boolean;
}

export interface User {
    _id: string;
    username: string;
    email: string;
    password: string;
    profilePicture?: string;
    bio?: string;
    createdAt?: string;
    firstName?: string;
    lastName?: string;
    boards?: Board[];
}
