import {AppService} from './app.service';

describe('AppService', () => {
    let appService: AppService;

    beforeEach(() => {
        appService = new AppService();
    });

    it('should exist', async () => {
        expect(appService).toBeDefined();
    });
});