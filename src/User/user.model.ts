import {Schema} from 'mongoose';
import {isEmail} from 'validator';

export const UserModel = new Schema({
        description: String,
        createdAt: {
            type: Date,
            default: Date.now,
        },
        email: {
            type: String,
            required: true,
            unique: true,
            minlength: 6,
            maxlength: 60,
            validate: [isEmail, 'Please fill a valid email address'],
        },
        firstName: {
            type: String,
            default: 'King',
        },
        lastName: {
            type: String,
            default: 'Kong',
        },
        password: {
            type: String,
            required: true,
        },
        profilePicture: {
            type: String,
            default: 'https://a.wattpad.com/useravatar/Nosacz_Janusz.128.77582.jpg',
        },
        updatedAt: {
            type: Date,
            default: Date.now,
        },
        username: {
            type: String,
            required: true,
            unique: true,
            minlength: 6,
            maxlength: 25,
        },
        boards: [{
            type: Schema.Types.ObjectId,
            ref: 'Board',
        }],
});
