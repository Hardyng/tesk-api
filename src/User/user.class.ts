import {Board, User} from '../interfaces';
import {Document} from 'mongoose';
import {MongoDocumentInstance} from '../shared/classes/MongoDocumentInstance';

export class UserInstance extends MongoDocumentInstance<User> {
    readonly mutation = {
        delete: async (): Promise<UserInstance> => {
            await this.mongoModel.remove();
            return this;
        },
        update: {
            instance: async (obj: User): Promise<UserInstance> => {
                await this.mongoModel.update(obj);
                return this;
            },
        },
        pushOne: {
            board: async (board: Board): Promise<UserInstance> => {
                await this.mongoModel.update({$push: {boards: board}});
                return this;
            },
        },
    };

    constructor(protected mongoModel: User & Document) {
        super(mongoModel);
    }
}
