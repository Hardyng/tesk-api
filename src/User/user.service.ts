import {Document, Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {User} from '../interfaces';
import {UserInstance} from './user.class';

@Injectable()
export class UserService {
    constructor(@InjectModel('User') public readonly model: Model<User & Document>) {
    }

    public async one(user: Partial<User>): Promise<UserInstance> {
        return new UserInstance(await this.model.findOne(user));
    }

    public async all(): Promise<UserInstance[]> {
        return (await this.model.find()).map(_us => new UserInstance(_us));
    }

    public async create(objectToCreate: Partial<User>): Promise<UserInstance> {
        return new UserInstance(await new this.model(objectToCreate).save());
    }
}