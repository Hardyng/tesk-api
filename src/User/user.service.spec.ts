import {UserService} from './user.service';
import {Test} from '@nestjs/testing';
import {MongooseModule} from '@nestjs/mongoose';
import {MockTestingService} from '../mock-testing/mock-testing.service';
import {UserModel} from './user.model';
jest.setTimeout(20000);

describe('UserService', () => {
    let service: UserService;

    beforeAll(async () => {
        await MockTestingService.getConfig();
        const module = await Test.createTestingModule({
            providers: [
                UserService,
            ],
            imports: [
                MongooseModule.forRoot(
                    MockTestingService.config.mongoUri,
                    {
                        useNewUrlParser: true,
                    }),
                MongooseModule.forFeature([
                    {
                        name: 'User',
                        schema: UserModel,
                    },
                ]),
            ],
        }).compile();

        service = module.get<UserService>(UserService);
    });
    afterAll(async () => {
        await MockTestingService.endTesting();
    });
    it('should exist', async () => {
        expect(service).toBeDefined();
    });
    it('should return all users', async () => {
        expect(await service.all()).toEqual([]);
    });
    it('should create and return specific user', async () => {
        const user = {
            username: 'testowy',
            email: 'testowy@gmail.com',
            password: '123456',
        };
        expect(await service.one(user)).toBeNull();
        await service.create(user);
        expect(await service.one(user)).toHaveProperty('email', user.email);
    });
    it('should update user', async () => {

        const user = {
            username: 'testowy2',
            email: 'testowy2@gmail.com',
            password: '123456',
        };
        await service.create(user);
        expect(await service.one(user)).toHaveProperty('email', user.email);
        await service.update(user, {email: 'test2'});
        expect(await service.one({email: user.email})).toBeNull();
        expect(await service.one({email: 'test2'})).toHaveProperty('email', 'test2');
    });
    it('should delete user', async () => {
        const initialLength = (await service.all()).length;
        const user = {
            username: 'testowy3',
            email: 'testowy3@gmail.com',
            password: '123456',
        };
        await service.create(user);
        expect(await service.all()).toHaveLength(initialLength + 1);
        await service.delete(user);
        expect(await service.all()).toHaveLength(initialLength);
    });

});