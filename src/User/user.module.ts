import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {UserModel} from './user.model';
import {UserService} from './user.service';
import {UserResolver} from './user.resolver';

@Module({
    providers: [
        UserService,
        UserResolver],
    exports: [
        UserService,
    ],
    imports: [MongooseModule.forFeature([{name: 'User', schema: UserModel}])],
})
export class UserModule {
}
