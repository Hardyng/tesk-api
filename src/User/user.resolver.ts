import {Args, Parent, Query, ResolveProperty, Resolver} from '@nestjs/graphql';
import {UserService} from './user.service';
import {Board, User} from '../interfaces';
import {Document} from 'mongoose';
import {UseGuards} from '@nestjs/common';
import {GqlAuthGuard} from '../auth/gqlAuth.guard';
import {omit, partialRight} from 'lodash';
import {map} from 'lodash/fp';
import {UserDecorator} from '../shared/user.decorator';

export const omitPassword = partialRight(omit, ['password']);

@Resolver('User')
export class UserResolver {
    constructor(public readonly service: UserService) {}

    @Query()
    @UseGuards(GqlAuthGuard)
    public async allUsers(): Promise<User[]> {
        const users = await this.service.all();
        return map(omitPassword)(users);
    }

    @Query()
    @UseGuards(GqlAuthGuard)
    public async oneUser(@Args('determine') determine: Partial<User>): Promise<User> {
        return omitPassword(await this.service.one(determine));
    }
    @ResolveProperty('boards')
    @UseGuards(GqlAuthGuard)
    async boards(@Parent() parent: User & Document): Promise<Board[]> {
        const pop = await parent.populate('boards').execPopulate();
        return pop.boards;
    }
}
