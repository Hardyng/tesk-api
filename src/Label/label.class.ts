import {Label} from '../interfaces';
import {Document} from 'mongoose';
import {MongoDocumentInstance} from '../shared/classes/MongoDocumentInstance';

export class LabelInstance extends MongoDocumentInstance<Label> {
    constructor(protected mongoModel: Label & Document) {
        super(mongoModel);
    }
}
