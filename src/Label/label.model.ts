import {Schema} from 'mongoose';

export const LabelModel = new Schema({
    name: {
        type: String,
        required: true,
        default: 'Default Label',
    },
    color: {
        type: String,
        required: true,
        default: '#ffffff',
    },
});
