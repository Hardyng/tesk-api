import {Inject, Injectable} from '@nestjs/common';
import {Document, Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {isNull} from 'lodash/fp';
import {Label, UpdateLabelInput} from '../interfaces';
import {BoardService} from '../Board/board.service';
import {LabelInstance} from './label.class';

@Injectable()
export class LabelService {
    constructor(@InjectModel('Label') public readonly model: Model<Label & Document>,
                @Inject('BoardService') private readonly boardService: BoardService,
    ) {
    }

    public async create(boardId: string, newLabelParam: UpdateLabelInput): Promise<LabelInstance> {
        const label = new this.model(newLabelParam);
        const boardInstance = await this.boardService.one({_id: boardId});
        await boardInstance.mutation.pushOne.label(label);
        return new LabelInstance(await label.save());
    }

    public async one(determineLabel: any): Promise<LabelInstance> {
        return new LabelInstance(await this.model.findOne(determineLabel));
    }
}