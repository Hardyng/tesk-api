import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {LabelModel} from './label.model';
import {LabelService} from './label.service';
import {LabelResolver} from './label.resolver';
import {BoardModule} from '../Board/board.module';

@Module({
    providers: [LabelService, LabelResolver],
    exports: [LabelService],
    imports: [
        BoardModule,
        MongooseModule.forFeature([{name: 'Label', schema: LabelModel}])],
})
export class LabelModule {
}
