import {Args, Mutation, Resolver} from '@nestjs/graphql';
import {LabelService} from './label.service';
import {UseGuards} from '@nestjs/common';
import {GqlAuthGuard} from '../auth/gqlAuth.guard';
import {CreateLabelInput, Label, UpdateLabelInput} from '../interfaces';

@Resolver('Label')
export class LabelResolver {
    constructor(public readonly service: LabelService) {
    }
    @Mutation()
    @UseGuards(GqlAuthGuard)
    async createLabel(@Args('boardId') boardId: string, @Args('mutation') createLabelPayload: CreateLabelInput): Promise<Label> {
        return (await this.service.create(boardId, createLabelPayload)).instance;
    }
    @Mutation()
    @UseGuards(GqlAuthGuard)
    async updateLabel(@Args('mutation') updateLabelPayload: UpdateLabelInput): Promise<Label> {
        const labelInstance = await this.service.one({_id: updateLabelPayload._id});
        return (await labelInstance.mutation.update.instance(updateLabelPayload)).instance;
    }

    @Mutation()
    @UseGuards(GqlAuthGuard)
    async deleteLabel(@Args('_id') _id: string): Promise<Label> {
        const labelInstance = await this.service.one({_id});
        return (await labelInstance.mutation.delete()).instance;
    }
}
