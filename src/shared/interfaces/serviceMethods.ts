export interface ServiceMethods<T> {
    one(...args): Promise<T>;

    all(...args): Promise<T[]>;

    create(...args): Promise<T>;
}
