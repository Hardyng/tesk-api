import {Document} from 'mongoose';

export abstract class MongoDocumentInstance<T> {
    readonly mutation = {
        delete: async (...args): Promise<MongoDocumentInstance<T>> => {
            await this.mongoModel.remove();
            return this;
        },
        update: {
            instance: async (obj: T, ...args): Promise<MongoDocumentInstance<T>> => {
                await this.mongoModel.update(obj);
                return this;
            },
        },
        pushOne: {},
    };

    protected constructor(protected mongoModel: T & Document) {
    }

    public get instance(): T {
        return this.mongoModel;
    }
}
