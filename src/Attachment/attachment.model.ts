import {Schema} from 'mongoose';

export const AttachmentModel = new Schema({
    name: {
        type: String,
        required: true,
        default: 'Default Attachment',
    },
    created: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    url: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: true,
        enum: ['IMAGE', 'LINK'],
        default: 'IMAGE',
    },
    card: {
        type: Schema.Types.ObjectId,
        ref: 'Card',
    },
});
