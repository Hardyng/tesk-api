import {Attachment} from '../interfaces';
import {Document} from 'mongoose';
import {MongoDocumentInstance} from '../shared/classes/MongoDocumentInstance';

export class AttachmentInstance extends MongoDocumentInstance<Attachment> {
    constructor(protected mongoModel: Attachment & Document) {
        super(mongoModel);
    }
}
