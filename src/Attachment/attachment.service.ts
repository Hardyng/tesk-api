import {Inject, Injectable} from '@nestjs/common';
import {Document, Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {isNull} from 'lodash/fp';
import {ActionType, ActivityType, Attachment, User} from '../interfaces';
import {ActivityService} from '../Activity/activity.service';
import {AttachmentInstance} from './attachment.class';

@Injectable()
export class AttachmentService {
    constructor(@InjectModel('Attachment') public readonly model: Model<Attachment & Document>,
                @Inject('ActivityService') public readonly activityService: ActivityService,
    ) {

    }

    public async create(cardId: string, name: string, url: string, user: User & Document): Promise<AttachmentInstance> {
        const attachment = new this.model({name, url, card: cardId});
        await this.activityService.createActivity(user, ActivityType.ATTACHMENT, ActionType.CREATE, String(attachment.card));
        return new AttachmentInstance(await attachment.save());
    }

    public async one(determine: any): Promise<AttachmentInstance> {
        return new AttachmentInstance(await this.model.findOne(determine));
    }
}