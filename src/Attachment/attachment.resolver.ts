import {Args, Mutation, Resolver} from '@nestjs/graphql';
import {AttachmentService} from './attachment.service';
import {Inject, UseGuards} from '@nestjs/common';
import {GqlAuthGuard} from '../auth/gqlAuth.guard';
import {Attachment, User} from '../interfaces';
import {UserDecorator} from '../shared/user.decorator';
import {Document} from 'mongoose';
import {CardService} from '../Card/card.service';

@Resolver('Attachment')
export class AttachmentResolver {
    constructor(public readonly service: AttachmentService,
                @Inject('CardService') public readonly cardService: CardService,
    ) {
    }

    @Mutation()
    @UseGuards(GqlAuthGuard)
    public async createAttachment(
        @Args('cardId') cardId: string,
        @Args('name') name: string,
        @Args('url') url: string,
        @UserDecorator() user: User & Document,
    ): Promise<Attachment> {
        const attachment = await this.service.create(cardId, name, url, user);
        (await (await this.cardService.one({_id: cardId})).mutation.pushOne.attachment(attachment.instance));
        return attachment.instance;
    }

    @Mutation()
    @UseGuards(GqlAuthGuard)
    public async deleteAttachment(@Args('attachmentId') attachmentId: string, @UserDecorator() user: User & Document): Promise<Attachment> {
        return (await (await this.service.one({_id: attachmentId})).mutation.delete()).instance;
    }
}
