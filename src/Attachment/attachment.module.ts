import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {AttachmentModel} from './attachment.model';
import {AttachmentService} from './attachment.service';
import {AttachmentResolver} from './attachment.resolver';
import {CardModule} from '../Card/card.module';
import {ActivityModule} from '../Activity/activity.module';

@Module({
    providers: [AttachmentService, AttachmentResolver],
    exports: [AttachmentService],
    imports: [
        ActivityModule,
        CardModule,
        MongooseModule.forFeature([{name: 'Attachment', schema: AttachmentModel}])],
})
export class AttachmentModule {
}
