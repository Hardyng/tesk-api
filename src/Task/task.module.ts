import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {TaskModel} from './task.model';
import {TaskService} from './task.service';
import {TaskResolver} from './task.resolver';
import {CheckListModule} from '../CheckList/checkList.module';

@Module({
    providers: [TaskService, TaskResolver],
    exports: [TaskService],
    imports: [
        CheckListModule,
        MongooseModule.forFeature([{name: 'Task', schema: TaskModel}])],
})
export class TaskModule {
}
