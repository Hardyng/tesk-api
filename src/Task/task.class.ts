import {Task} from '../interfaces';
import {Document} from 'mongoose';
import {MongoDocumentInstance} from '../shared/classes/MongoDocumentInstance';

export class TaskInstance extends MongoDocumentInstance<Task> {
    constructor(protected mongoModel: Task & Document) {
        super(mongoModel);
    }
}
