import {Injectable} from '@nestjs/common';
import {Document, Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {isNull} from 'lodash/fp';
import {Task} from '../interfaces';
import {TaskInstance} from './task.class';

@Injectable()
export class TaskService {
    constructor(@InjectModel('Task') public readonly model: Model<Task & Document>,
    ) {
    }

    public async create(checkListId: string, name: string): Promise<TaskInstance> {
        const task = new this.model({name});
        return new TaskInstance(await task.save());
    }

    public async one(checkListDetermine: any): Promise<TaskInstance> {
        return new TaskInstance(await this.model.findOne(checkListDetermine));
    }
}