import {Schema} from 'mongoose';

export const TaskModel = new Schema({
    name: {
        type: String,
        required: true,
        default: 'Very important stuff',
    },
    done: {
        type: Boolean,
        default: false,
    },
});
