import {Args, Mutation, Resolver} from '@nestjs/graphql';
import {TaskService} from './task.service';
import {Inject, UseGuards} from '@nestjs/common';
import {GqlAuthGuard} from '../auth/gqlAuth.guard';
import {Task, UpdateTaskInput} from '../interfaces';
import {CheckListService} from '../CheckList/checkList.service';

@Resolver('Task')
export class TaskResolver {
    constructor(public readonly service: TaskService,
                @Inject('CheckListService') public readonly checkListService: CheckListService,
    ) {
    }

    @Mutation()
    @UseGuards(GqlAuthGuard)
    async addTask(@Args('checkListId') checkListId: string, @Args('name') name: string): Promise<Task> {
        const taskInstance = await this.service.create(checkListId, name);
        (await this.checkListService.one({_id: checkListId})).mutation.pushOne.task(taskInstance.instance);
        return taskInstance.instance;
    }

    @Mutation()
    @UseGuards(GqlAuthGuard)
    async removeTask(@Args('id') id: string): Promise<Task> {
        return (await (await this.service.one({_id: id})).mutation.delete()).instance;
    }

    @Mutation()
    @UseGuards(GqlAuthGuard)
    async updateTask(@Args('id') id: string, @Args('mutation') mutation: UpdateTaskInput): Promise<Task> {
        return (await (await this.service.one({_id: id})).mutation.update.instance(mutation)).instance;
    }
}
