import {Module} from '@nestjs/common';
import {AppService} from './app.service';
import {GraphQLModule} from '@nestjs/graphql';
import {UserModule} from './User/user.module';
import {MongooseModule} from '@nestjs/mongoose';
import {join} from 'path';
import {AuthModule} from './auth/auth.module';
import {BoardModule} from './Board/board.module';
import {CardListModule} from './CardList/cardList.module';
import * as mongoose from 'mongoose';
import {CardModule} from './Card/card.module';
import {CommentModule} from './Comment/comment.module';
import {CheckListModule} from './CheckList/checkList.module';
import {TaskModule} from './Task/task.module';
import {LabelModule} from './Label/label.module';
import {AttachmentModule} from './Attachment/attachment.module';

if (!process.env['USERNAME']) {
    require('dotenv').config();
}
const { ObjectId } = mongoose.Types;

ObjectId.prototype.valueOf = function() {
    return this.toString();
};

@Module({
    imports: [
        MongooseModule.forRoot(
            `mongodb://${process.env['USERNAME']}:${process.env['PASSWORD']}@ds237363.mlab.com:37363/ultimateru`,
            {
                useNewUrlParser: true,
                useCreateIndex: true,
            }),
        GraphQLModule.forRoot({
            context: ({req}) => ({req}),
            typePaths: ['./**/*.graphql'],
            definitions: {
                path: join(process.cwd(), 'src/interfaces.ts'),
            },
            introspection: true,
            playground: true,
            debug: true,
        }),
        BoardModule,
        CardListModule,
        UserModule,
        AuthModule,
        CardModule,
        CommentModule,
        CheckListModule,
        TaskModule,
        LabelModule,
        AttachmentModule,
    ],
    controllers: [],
    providers: [AppService],
})
export class AppModule {
}
