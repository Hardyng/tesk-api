import {Injectable} from '@nestjs/common';

const {MongoMemoryServer} = require('mongodb-memory-server');

@Injectable()
export class MockTestingService {
    public static config = {
        mongoDBName: 'jest',
        mongoUri: null,
    };
    public static getConfig = async () => {
        const uri = await MockTestingService.mongoServer.getConnectionString();
        MockTestingService.config.mongoUri = uri;
    }
    public static endTesting = async () => {
        await MockTestingService.mongoServer.stop();
    }
    private static readonly mongoServer = new MongoMemoryServer();
}
