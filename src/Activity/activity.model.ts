import {Schema} from 'mongoose';

export const ActivityModel = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    date: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    type: {
        type: String,
        required: true,
        enum: ['ATTACHMENT', 'COMMENT'],
    },
    action: {
        type: String,
        required: true,
        enum: ['CREATE', 'DELETE', 'UPDATE'],
    },
});
