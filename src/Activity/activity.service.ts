import {Inject, Injectable} from '@nestjs/common';
import {Document, Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {isNull} from 'lodash/fp';
import {ActionType, Activity, ActivityType, User} from '../interfaces';
import {CardService} from '../Card/card.service';
import {ActivityInstance} from './activity.class';

@Injectable()
export class ActivityService {
    constructor(@InjectModel('Activity') public readonly model: Model<Activity & Document>,
                @Inject('CardService') public readonly cardService: CardService,
    ) {
    }

    async createActivity(user: User, type: ActivityType, action: ActionType, cardId: string): Promise<ActivityInstance> {
        const activity = new this.model({user, type, action});
        await this.cardService.addActivity(cardId, activity);
        return new ActivityInstance(await activity.save());
    }
}