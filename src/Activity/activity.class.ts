import {Activity} from '../interfaces';
import {Document} from 'mongoose';
import {MongoDocumentInstance} from '../shared/classes/MongoDocumentInstance';

export class ActivityInstance extends MongoDocumentInstance<Activity> {
    constructor(protected mongoModel: Activity & Document) {
        super(mongoModel);
    }
}
