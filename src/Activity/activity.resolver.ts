import {Parent, ResolveProperty, Resolver} from '@nestjs/graphql';
import {ActivityService} from './activity.service';
import {UseGuards} from '@nestjs/common';
import {GqlAuthGuard} from '../auth/gqlAuth.guard';
import {Activity, User} from '../interfaces';
import {Document} from 'mongoose';

@Resolver('Activity')
export class ActivityResolver {
    constructor(public readonly service: ActivityService) {
    }

    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async user(@Parent() parent: Activity & Document): Promise<User> {
        const populated = await parent.populate('user').execPopulate();
        return populated.user;
    }
}
