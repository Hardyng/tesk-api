import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {ActivityModel} from './activity.model';
import {ActivityService} from './activity.service';
import {ActivityResolver} from './activity.resolver';
import {CardModule} from '../Card/card.module';

@Module({
    providers: [ActivityService, ActivityResolver],
    exports: [ActivityService],
    imports: [
        CardModule,
        MongooseModule.forFeature([{name: 'Activity', schema: ActivityModel}])],
})
export class ActivityModule {
}
