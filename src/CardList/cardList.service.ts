import {forwardRef, Inject, Injectable} from '@nestjs/common';
import {isNull} from 'lodash/fp';
import {CardList, GetOneBoardInput, GetOneCardListInput} from '../interfaces';
import {Document, Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
import {BoardService} from '../Board/board.service';
import {CardService} from '../Card/card.service';
import {CardListInstance} from './cardList.class';

@Injectable()
export class CardListService {
    constructor(@InjectModel('CardList') public readonly model: Model<CardList & Document>,
                @Inject('BoardService') private readonly boardService: BoardService,
                @Inject(forwardRef(() => CardService)) private readonly cardService: CardService,
    ) {
    }

    public async one(determineCardList: GetOneCardListInput): Promise<CardListInstance> {
        return new CardListInstance(await this.model.findOne(determineCardList));
    }

    public async create(determineBoard: GetOneBoardInput, name: string): Promise<CardListInstance> {
        const list = new this.model({name});
        const boardInstance = await this.boardService.one(determineBoard);
        await boardInstance.mutation.pushOne.list(list);
        return new CardListInstance(await list.save());
    }

}