import {Args, Mutation, Parent, ResolveProperty, Resolver} from '@nestjs/graphql';
import {CardListService} from './cardList.service';
import {Card, CardList, GetOneBoardInput, GetOneCardListInput} from '../interfaces';
import {UseGuards} from '@nestjs/common';
import {GqlAuthGuard} from '../auth/gqlAuth.guard';
import {Document} from 'mongoose';

@Resolver('CardList')
export class CardListResolver {
    constructor(public readonly service: CardListService) {
    }

    @Mutation()
    @UseGuards(GqlAuthGuard)
    async addCardList(@Args('determineBoard') determineBoard: GetOneBoardInput, @Args('name') name: string): Promise<CardList> {
        return (await this.service.create(determineBoard, name)).instance;
    }

    @Mutation()
    @UseGuards(GqlAuthGuard)
    async removeCardList(@Args('determineCardList') determineCardList: GetOneCardListInput): Promise<CardList> {
        return (await (await this.service.one(determineCardList)).mutation.delete()).instance;
    }

    @ResolveProperty()
    @UseGuards(GqlAuthGuard)
    async cards(@Parent() parent: CardList & Document): Promise<Card[]> {
        const populatedParent = await parent.populate('cards').execPopulate();
        return populatedParent.cards;
    }

}
