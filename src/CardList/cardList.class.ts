import {Card, CardList} from '../interfaces';
import {Document} from 'mongoose';
import {MongoDocumentInstance} from '../shared/classes/MongoDocumentInstance';

export class CardListInstance extends MongoDocumentInstance<CardList> {
    readonly mutation = {
        delete: async (): Promise<CardListInstance> => {
            await this.mongoModel.remove();
            return this;
        },
        update: {
            instance: async (obj: CardList): Promise<CardListInstance> => {
                await this.mongoModel.update(obj);
                return this;
            },
        },
        pushOne: {
            card: async (card: Card): Promise<CardListInstance> => {
                return await this.mongoModel.update({$push: {cards: card}});
            },
        },
    };

    constructor(protected mongoModel: CardList & Document) {
        super(mongoModel);
    }
}
