import {forwardRef, Module} from '@nestjs/common';
import {CardListService} from './cardList.service';
import {MongooseModule} from '@nestjs/mongoose';
import {CardListModel} from './cardList.model';
import {CardListResolver} from './cardList.resolver';
import {UserModule} from '../User/user.module';
import {BoardModule} from '../Board/board.module';
import {CardModule} from '../Card/card.module';

@Module({
    providers: [CardListService, CardListResolver],
    exports: [CardListService],
    imports: [
        // UserModule,
        BoardModule,
        forwardRef(() => CardModule),
        MongooseModule.forFeature([{name: 'CardList', schema: CardListModel}])],
})
export class CardListModule {
}
