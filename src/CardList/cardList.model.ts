import {Schema} from 'mongoose';

export const CardListModel = new Schema({
    name: {
        type: String,
        default: 'Untitled Card List',
    },
    cards: [{
        type: Schema.Types.ObjectId,
        ref: 'Card',
        default: [],
    }],
});
