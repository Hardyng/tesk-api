
## Description
Project of simple tasker api written in [nestjs](https://nestjs.com/), graphql and mongodb.

To run:
```bash
$ npm install
$ npm run start:dev
```
And open localhost:3000/graphql

